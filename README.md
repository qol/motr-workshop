# MoTR Workshop
#### Model-Twin Randomization

This jupyter notebook will help you setting up the MoTR code.

If you want to use it with different data you are free to do so. Simply edit the name of the input file.

#### **LAST UPDATE: October 2024**

## Demo data

In the folder "demo_data" of this repository you will find three demo data files. Those were collected with a real device in the real world. Some missing values were already filled in using ML techniques. Those files serve to illustrate the differences in conclusions for the same hypothesis between different individuals.

More public data can be found here: http://qol-data.unige.ch.

## What is MoTR?

MoTR ("motor") is a new causal inference method that artificially emulates an n-of-1 randomized trial (i.e., the gold standard due to randomization) from the n-of-1 observational study dataset.
It does so by first modeling the outcome of interest as a function of the exposure of interest, along with an individual's assumed recurring confounders (i.e., daily observed variables thought to influence or affect both the exposure and the outcome).
MoTR then randomly shuffles (i.e., permutes) the exposures, which were originally only observed, thereby simulating an n-of-1 randomized trial.
This allows us to infer more accurately a suggested effect of daily stressors beyond just correlation.

### How does it work?

Briefly, model-twin randomization is a Monte Carlo method that randomly shuﬄes the exposure vector over all periods, predicts potential outcomes under each treatment level, adds random noise for better statistical comparison, compares the average predicted noisy outcomes under each treatment level, and then repeats this procedure multiple times until convergence. 

## MoTR-python v1 (Linear Regression)

This workshop uses the Python implementation of the Model-Twin Randomization (MoTR) method (may have changes not yet published there, though).
The public version of it is available in https://gitlab.unige.ch/qol/MoTR-python/-/tree/main). (not necessarily the same version as this one — it may happen that this workshop has the latest version)
Future versions will be developed and published there (this workshop will not necessarily be kept updated).

We thank you for acknowledging our work by citing: 

**Matias I., Daza E. J., and Wac K. "What possibly affects nighttime heart rate? Conclusions from N-of-1 observational data", SAGE Digital Health, 24 August 2022.**
https://doi.org/10.1177%2F20552076221120725.

**Daza, E. J., Matias I., and Schneider, L. (2024). Model-Twin Randomization (MoTR) for Estimating One's Own Recurring Individual Treatment Effect. ArXiv.**
https://arxiv.org/abs/2208.00739

## Disclaimer

This script is provided as is and should not be used to infer self-medication and/or life-threatening self-intervention.